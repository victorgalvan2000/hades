import sys
sys.path.append('../../')
from app.config.wsgi import *
from core.erp.models import Type, Employee


# Listar
# query = Type.objects.all()
# print(query)

# Insertar
# t = Type()
# t.name = 'Prueba'
# t.save()

# Edicion
# try:
#     t = Type.objects.get(id=1)
#     t.name = 'asjasjasj'
#     t.save( )
# except Exception as e:
#     print(e)


# Eliminacion
# t = Type.objects.get(id=1)
# t.delete()

# Selecion con filtros
# try:
#     obj = Type.objects.filter(name__icontains='pr')
#     print(obj)
# except Exception as e:
#     print(e)

# Selecion con filtros 2
# try:
#     obj = Type.objects.filter(name__startswith='P')
#     print(obj)
# except Exception as e:
#     print(e)

# Selecion con filtros 3
# try:
#     obj = Type.objects.filter(name__endswith='a')
#     print(obj)
# except Exception as e:
#     print(e)

# Selecion con filtros 4
# try:
#     obj = Type.objects.filter(name__in=['Vida','hola']).count()
#     print(obj)
# except Exception as e:
#     print(e)

# Selecion con filtros y arrojar el codigo de la consulta
# try:
#     obj = Type.objects.filter(name__contains='Terry').query
#     print(obj)
# except Exception as e:
#     print(e)

# Selecion con for
# try:
#     for i in Type.objects.filter(name__endswith='a'):
#         print(i.name)
# except Exception as e:
#     print(e)


# obj = Employee.objects.filter(type_id=1)
# print(obj)